FROM adoptopenjdk/openjdk11:alpine
MAINTAINER Pedro Bernardo <pedro.bernardo@avenuecode.com>

COPY target/*.jar school.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/school.jar"]
