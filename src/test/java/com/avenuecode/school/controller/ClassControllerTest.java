package com.avenuecode.school.controller;

import com.avenuecode.school.entity.Class;
import com.avenuecode.school.entity.Student;
import com.avenuecode.school.entity.Teacher;
import com.avenuecode.school.service.ClassService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;

@WebMvcTest(ClassController.class)
public class ClassControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ClassService classService;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserDetailsService userDetailsService;

    @WithMockUser
    @Test
    void getById_ShouldReturnClass() throws Exception {
        Mockito.when(classService.getById(1L)).thenReturn(this.returnClass());
        mockMvc.perform(get("/class/" + 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }

    @WithMockUser(value = "admin")
    @Test
    void getById_ShouldThrowException() throws Exception {
        Mockito.doThrow(new Exception("Class Not Found")).when(this.classService).getById(2L);

        Throwable exception = assertThrows(Exception.class, () ->
                mockMvc.perform(get("/class/" + 2L)));
        assertEquals("Class Not Found", exception.getCause().getMessage());
    }

    @WithMockUser(value = "admin")
    @Test
    void getAll_ShouldReturnArrayClass() throws Exception {

        Mockito.when(classService.getAll()).thenReturn(Arrays.asList(this.returnClass()));
        mockMvc.perform(get("/class/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(Arrays.asList(this.returnClass()).size())));
    }

    @WithMockUser(value = "admin")
    @Test
    void getAll_ShouldReturnEmptyArray() throws Exception {

        Mockito.when(classService.getAll()).thenReturn(new ArrayList<>());
        mockMvc.perform(get("/class/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(new ArrayList<>().size())));
    }

    @WithMockUser(value = "admin")
    @Test
    void save_ShouldSaveNewClass() throws Exception {

        Class classEntity = new Class();
        classEntity.setName("Software Engineer");
        classEntity.setTeacher(this.returnTeacher());
        classEntity.setStudents(this.returnStudents());

        Mockito.doReturn(this.returnClass()).when(classService).save(any(Class.class));

       mockMvc.perform(post("/class/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(classEntity)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value(this.returnClass().getName()))
                .andExpect(jsonPath("id").value(this.returnClass().getId()));
    }

    @WithMockUser(value = "admin")
    @Test
    public void update_ShouldUpdateClass() throws Exception {
        Mockito.doReturn(this.returnClass()).when(classService).update(any(Class.class), any(Long.class));

        mockMvc.perform(put("/class/" + 1L)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(this.returnClass())))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value(this.returnClass().getName()))
                .andExpect(jsonPath("id").value(this.returnClass().getId()));
    }

    @WithMockUser(value = "admin")
    @Test
    public void update_ShouldThrowException() throws Exception {

        Mockito.doThrow(new Exception("Class Not Found")).when(this.classService).update(any(Class.class), any(Long.class));

        Throwable exception = assertThrows(Exception.class, () ->
                        mockMvc.perform(put("/class/" + 1L)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(objectMapper.writeValueAsString(this.returnClass()))));
        assertEquals("Class Not Found", exception.getCause().getMessage());
    }

    @WithMockUser(value = "admin")
    @Test
    public void delete_ShouldDeleteClass() throws Exception {

        Mockito.when(classService.delete(1L)).thenReturn(1L);

        mockMvc.perform(delete("/class/" + 1L))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(1L));
    }

    @WithMockUser(value = "admin")
    @Test
    public void delete_ShouldThrowException() throws Exception {

        Mockito.doThrow(new Exception("Class Not Found")).when(this.classService).delete(1L);

        Throwable exception = assertThrows(Exception.class, () ->
                mockMvc.perform(delete("/class/" + 1L)));
        assertEquals("Class Not Found", exception.getCause().getMessage());
    }

    public Class returnClass() {
        Class classEntity = new Class();
        classEntity.setId(1L);
        classEntity.setName("Software Engineer");
        classEntity.setTeacher(this.returnTeacher());
        classEntity.setStudents(this.returnStudents());

        return classEntity;
    }

    public Teacher returnTeacher() {
        Teacher teacherEntity = new Teacher();
        teacherEntity.setId(1L);
        teacherEntity.setName("Pedro Bernardo");

        return teacherEntity;
    }

    public List<Student> returnStudents() {
        Student studentEntity1 = new Student();
        studentEntity1.setId(1L);
        studentEntity1.setName("Luis Guadagnin");
        studentEntity1.setBirthDate(LocalDateTime.of(1991, 12, 31, 10, 10));

        Student studentEntity2 = new Student();
        studentEntity2.setId(2L);
        studentEntity2.setName("Felipe Thiago");
        studentEntity2.setBirthDate(LocalDateTime.of(1990, 1, 20, 1, 59));

        List<Student> students = new ArrayList<>();

        students.add(studentEntity1);
        students.add(studentEntity2);

        return students;
    }
}