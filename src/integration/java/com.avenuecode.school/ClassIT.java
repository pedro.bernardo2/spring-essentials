package com.avenuecode.school;

import com.avenuecode.school.entity.Class;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@ExtendWith(SpringExtension.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ClassIT {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    private static Class classEntity;

    @Test
    @Order(1)
    @WithMockUser
    public void saveNewClass() throws Exception {

        Class newClass = new Class();
        newClass.setName("Software Engineer");

        MvcResult result = mockMvc.perform(post("/class/")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(newClass)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("name").value("Software Engineer"))
                .andExpect(jsonPath("id").exists())
                .andReturn();

        classEntity = objectMapper.readValue(result.getResponse().getContentAsString(), Class.class);
    }

    @Test
    @Order(2)
    @WithMockUser
    public void getById() throws Exception {

        var classReturned = new Class();

        MvcResult result = mockMvc.perform(get("/class/" + classEntity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        classReturned = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Class.class);

        assertEquals(classReturned.getId(), classEntity.getId());
        assertEquals(classReturned.getName(), classEntity.getName());
    }

    @Test
    @Order(3)
    @WithMockUser
    public void updateClass() throws Exception {

        var classReturned = new Class();

        classEntity.setName("Object Oriented Programming");

        MvcResult result = mockMvc.perform(put("/class/" + classEntity.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(classEntity)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andReturn();

        classReturned = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Class.class);

        assertEquals(classReturned.getId(), classEntity.getId());
        assertEquals(classReturned.getName(), classEntity.getName());
    }

    @Test
    @Order(4)
    @WithMockUser
    public void deleteClass() throws Exception {
        mockMvc.perform(delete("/class/" + classEntity.getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").value(classEntity.getId()));
    }
}