package com.avenuecode.school.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data
@Table(name = "class")
public class Class {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    private String name;

    @OneToOne
    private Teacher teacher;

    @OneToMany(targetEntity=Student.class, fetch=FetchType.EAGER)
    private List<Student> students;
}