package com.avenuecode.school.service;

import com.avenuecode.school.entity.Teacher;
import com.avenuecode.school.repository.TeacherRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class TeacherService {

    private TeacherRepository repository;

    @Autowired
    public TeacherService(TeacherRepository repository) {
        this.repository = repository;
    }

    public Teacher getById(Long id) throws Exception {
        return repository.findById(id).orElseThrow(() -> new Exception("Teacher Not Found"));
    }

    public List<Teacher> getAll() {
        return repository.findAll();
    }

    public Teacher save(Teacher entity) {
        return repository.save(entity);
    }

    public Teacher update(Teacher entity, Long id) throws Exception {
        Teacher teacherFound =  repository.findById(id).orElseThrow(() -> new Exception("Teacher Not Found"));
        teacherFound.setName(entity.getName().isEmpty() ? teacherFound.getName() : entity.getName());
        return repository.save(teacherFound);
    }

    public Long delete (Long id) throws Exception {
        Teacher teacherFound =  repository.findById(id).orElseThrow(() -> new Exception("Teacher Not Found"));
        try {
            repository.delete(teacherFound);
        } catch (Exception ex) {
            log.error("Error trying to delete the Teacher with Id: %s", id);
            throw new Exception("Cannot delete this Teacher");
        }
        return id;
    }
}
