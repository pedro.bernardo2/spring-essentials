package com.avenuecode.school.service;

import com.avenuecode.school.entity.Class;
import com.avenuecode.school.repository.ClassRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ClassService {

    private ClassRepository repository;

    @Autowired
    public ClassService(ClassRepository repository) {
        this.repository = repository;
    }

    public Class getById(Long id) throws Exception {
        return repository.findById(id).orElseThrow(() -> new Exception("Class Not Found"));
    }

    public List<Class> getAll() {
        return repository.findAll();
    }

    public Class save(Class entity) {
        return repository.save(entity);
    }

    public Class update(Class entity, Long id) throws Exception {
        Class classFound =  repository.findById(id).orElseThrow(() -> new Exception("Class Not Found"));

        classFound.setName(entity.getName().isEmpty() ? classFound.getName() : entity.getName());
        classFound.setTeacher(entity.getTeacher() != null ? entity.getTeacher() : classFound.getTeacher());
        classFound.setStudents(entity.getStudents() == null || entity.getStudents().isEmpty() ? classFound.getStudents() : entity.getStudents());
        return repository.save(classFound);
    }

    public Long delete (Long id) throws Exception {
        Class classFound =  repository.findById(id).orElseThrow(() -> new Exception("Class Not Found"));
        try {
            repository.delete(classFound);
        } catch (Exception ex) {
            log.error("Error trying to delete the Class with Id: %s", id);
            throw new Exception("Cannot delete this Class");
        }
        return id;
    }

}
