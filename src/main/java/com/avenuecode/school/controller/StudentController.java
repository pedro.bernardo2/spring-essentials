package com.avenuecode.school.controller;

import com.avenuecode.school.entity.Student;
import com.avenuecode.school.service.StudentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/student")
public class StudentController {

    private StudentService service;

    @Autowired
    public StudentController(StudentService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Student> getById(@PathVariable Long id) throws Exception {
        log.info("Request to get Student by the Id: %s", id);
        return ResponseEntity.ok(service.getById(id));
    }

    @GetMapping("")
    public ResponseEntity<List<Student>> getAll() {
        log.info("Request to get All Students");
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("")
    public ResponseEntity save(@RequestBody Student studentEntity) {
        log.info("Request to save a New Student");
        return ResponseEntity.ok(service.save(studentEntity));
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Student studentEntity, @PathVariable Long id) throws Exception {
        log.info("Request to update the Student with the Id: %s", id);
        return ResponseEntity.ok(service.update(studentEntity, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) throws Exception {
        log.info("Request to delete the Student with the Id: %s", id);
        return ResponseEntity.ok(service.delete(id));
    }

}
