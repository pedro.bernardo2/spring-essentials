package com.avenuecode.school.controller;

import com.avenuecode.school.entity.Teacher;
import com.avenuecode.school.service.TeacherService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private TeacherService service;

    @Autowired
    public TeacherController(TeacherService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Teacher> getById(@PathVariable Long id) throws Exception {
        log.info("Request to get Teacher by the Id: %s", id);
        return ResponseEntity.ok(service.getById(id));
    }

    @GetMapping("")
    public ResponseEntity<List<Teacher>> getAll() {
        log.info("Request to get All Teachers");
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping("")
    public ResponseEntity save(@RequestBody Teacher teacherEntity) {
        log.info("Request to save a New Teacher");
        return ResponseEntity.ok(service.save(teacherEntity));
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Teacher teacherEntity, @PathVariable Long id) throws Exception {
        log.info("Request to update the Teacher with the Id: %s", id);
        return ResponseEntity.ok(service.update(teacherEntity, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) throws Exception {
        log.info("Request to delete the Teacher with the Id: %s", id);
        return ResponseEntity.ok(service.delete(id));
    }
}