package com.avenuecode.school.service;

import com.avenuecode.school.entity.Student;
import com.avenuecode.school.repository.StudentRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class StudentService {

    private StudentRepository repository;

    @Autowired
    public StudentService(StudentRepository repository) {
        this.repository = repository;
    }

    public Student getById(Long id) throws Exception {
        return repository.findById(id).orElseThrow(() -> new Exception("Student Not Found"));
    }

    public List<Student> getAll() {
        return repository.findAll();
    }

    public Student save(Student entity) {
        return repository.save(entity);
    }

    public Student update(Student entity, Long id) throws Exception {
        Student studentFound =  repository.findById(id).orElseThrow(() -> new Exception("Student Not Found"));
        studentFound.setName(entity.getName().isEmpty() ? studentFound.getName() : entity.getName());
        return repository.save(studentFound);
    }

    public Long delete (Long id) throws Exception {
        Student studentFound =  repository.findById(id).orElseThrow(() -> new Exception("Student Not Found"));
        try {
            repository.delete(studentFound);
        } catch (Exception ex) {
            log.error("Error trying to delete the Student with Id: %s", id);
            throw new Exception("Cannot delete this Student");
        }
        return id;
    }
}
