package com.avenuecode.school.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import static com.avenuecode.school.security.ApplicationUserPermission.*;
import static com.avenuecode.school.security.ApplicationUserRole.*;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    private final PasswordEncoder passwordEncoder;

    public ApplicationSecurityConfig(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET,"/teacher").hasAnyAuthority(TEACHER_WRITE.getPermission())
                .antMatchers(HttpMethod.POST,"/teacher").hasAnyAuthority(TEACHER_WRITE.getPermission())
                .antMatchers(HttpMethod.PUT,"/teacher").hasAnyAuthority(TEACHER_WRITE.getPermission())
                .antMatchers(HttpMethod.DELETE,"/teacher").hasAnyAuthority(TEACHER_WRITE.getPermission())
                .antMatchers(HttpMethod.GET,"/class").hasAnyAuthority(CLASS_WRITE.getPermission())
                .antMatchers(HttpMethod.POST,"/class").hasAnyAuthority(CLASS_WRITE.getPermission())
                .antMatchers(HttpMethod.PUT,"/class").hasAnyAuthority(CLASS_WRITE.getPermission())
                .antMatchers(HttpMethod.DELETE,"/class").hasAnyAuthority(CLASS_WRITE.getPermission())
                .anyRequest()
                .authenticated()
                .and()
                .httpBasic();
    }

    @Bean
    protected UserDetailsService userDetailsService() {
        UserDetails student = User.builder().username("student")
                .password(passwordEncoder.encode("password123"))
                .authorities(STUDENT.getGrantedAuthorities())
                .build();

        UserDetails teacher = User.builder().username("teacher")
                .password(passwordEncoder.encode("password123"))
                .authorities(TEACHER.getGrantedAuthorities())
                .build();

        UserDetails admin = User.builder().username("admin")
                .password(passwordEncoder.encode("password123"))
                .authorities(ADMIN.getGrantedAuthorities())
                .build();

        return new InMemoryUserDetailsManager(student, teacher, admin);
    }
}