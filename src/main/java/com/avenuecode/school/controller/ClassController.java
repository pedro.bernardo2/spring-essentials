package com.avenuecode.school.controller;

import com.avenuecode.school.entity.Class;
import com.avenuecode.school.service.ClassService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/class")
public class ClassController {

    private ClassService service;

    @Autowired
    public ClassController(ClassService service) {
        this.service = service;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Class> getById(@PathVariable Long id) throws Exception {
        log.info("Request to get Class by the Id: %s", id);
        return ResponseEntity.ok(service.getById(id));
    }

    @GetMapping
    public ResponseEntity<List<Class>> getAll() {
        log.info("Request to get All Classes");
        return ResponseEntity.ok(service.getAll());
    }

    @PostMapping
    public ResponseEntity save(@RequestBody Class classEntity) {
        log.info("Request to save a New Class");
        return ResponseEntity.ok(service.save(classEntity));
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@RequestBody Class classEntity, @PathVariable Long id) throws Exception {
        log.info("Request to update the Class with the Id: %s", id);
        return ResponseEntity.ok(service.update(classEntity, id));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@PathVariable Long id) throws Exception {
        log.info("Request to delete the Class with the Id: %s", id);
        return ResponseEntity.ok(service.delete(id));
    }
}