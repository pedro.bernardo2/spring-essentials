package com.avenuecode.school.service;

import com.avenuecode.school.entity.Class;
import com.avenuecode.school.entity.Student;
import com.avenuecode.school.entity.Teacher;
import com.avenuecode.school.repository.ClassRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
public class ClassServiceTest {

    @InjectMocks
    private ClassService classService;

    @Mock
    private ClassRepository classRepository;

    @Test
    public void getById_ShouldReturnClass() throws Exception {

        Mockito.doReturn(Optional.of(this.returnClass())).when(this.classRepository).findById(any(Long.class));

        Class classReturned = this.classService.getById(1L);

        assertNotNull(classReturned);
        assertEquals(classReturned.getId(), this.returnClass().getId());
        assertEquals(classReturned.getName(), this.returnClass().getName());
        assertEquals(classReturned.getClass(), this.returnClass().getClass());
    }

    @Test
    public void getById_ShouldReturnException() {

        Mockito.doThrow(new NoSuchElementException()).when(this.classRepository).findById(2L);

        assertThrows(NoSuchElementException.class, () -> this.classService.getById(2L));

    }

    @Test
    public void getAll_ShouldReturnArray() {

        Mockito.doReturn(Arrays.asList(this.returnClass())).when(this.classRepository).findAll();

        List<Class> classes = this.classService.getAll();

        assertFalse(classes.isEmpty());
    }

    @Test
    public void getAll_ShouldReturnEmptyArray() {

        Mockito.doReturn(new ArrayList<>()).when(this.classRepository).findAll();

        List<Class> classes = this.classService.getAll();

        assertTrue(classes.isEmpty());
    }

    @Test
    public void save_ShouldSaveNewClass() {

        Mockito.doReturn(this.returnClass()).when(this.classRepository).save(any(Class.class));

        Class classEntity = this.classService.save(new Class());

        assertNotNull(classEntity);
        assertEquals(classEntity.getName(), this.returnClass().getName());
        assertEquals(classEntity.getClass(), this.returnClass().getClass());

    }

    @Test
    public void update_ShouldUpdateClass() throws Exception {

        Mockito.doReturn(this.returnClass()).when(this.classRepository).save(any(Class.class));
        Mockito.doReturn(Optional.of(this.returnClass())).when(this.classRepository).findById(1L);

        Class classEntity = this.classService.update(this.returnClass(), 1L);

        assertNotNull(classEntity);
        assertEquals(classEntity.getId(), this.returnClass().getId());
        assertEquals(classEntity.getName(), this.returnClass().getName());

    }

    @Test
    public void update_ShouldThrowException() {

        Mockito.doThrow(NoSuchElementException.class).when(this.classRepository).findById(any(Long.class));

        assertThrows(NoSuchElementException.class, () -> this.classService.getById(1L));

    }

    @Test
    public void delete_ShouldDeleteClass() throws Exception {

        Mockito.doReturn(Optional.of(this.returnClass())).when(this.classRepository).findById(1L);

        Long id = this.classService.delete(1L);

        assertNotNull(id);
        assertEquals(id, 1L);

    }

    @Test
    public void delete_ShouldThrowException() {

        Mockito.doThrow(NoSuchElementException.class).when(this.classRepository).findById(1L);

        assertThrows(NoSuchElementException.class, () -> this.classService.delete(1L));
    }

    public Class returnClass() {
        Class classEntity = new Class();
        classEntity.setId(1L);
        classEntity.setName("Software Engineer");
        classEntity.setTeacher(this.returnTeacher());
        classEntity.setStudents(this.returnStudents());

        return classEntity;
    }

    public Teacher returnTeacher() {
        Teacher teacherEntity = new Teacher();
        teacherEntity.setId(1L);
        teacherEntity.setName("Pedro Bernardo");

        return teacherEntity;
    }

    public List<Student> returnStudents() {
        Student studentEntity1 = new Student();
        studentEntity1.setId(1L);
        studentEntity1.setName("Luis Guadagnin");
        studentEntity1.setBirthDate(LocalDateTime.of(1991, 12, 31, 10, 10));

        Student studentEntity2 = new Student();
        studentEntity2.setId(2L);
        studentEntity2.setName("Felipe Thiago");
        studentEntity2.setBirthDate(LocalDateTime.of(1990, 1, 20, 1, 59));

        List<Student> students = new ArrayList<>();

        students.add(studentEntity1);
        students.add(studentEntity2);

        return students;
    }
}